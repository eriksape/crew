@php
    $table_cols = [
        'control' => [ 'className' =>  'details-control', 'orderable' =>  false, 'searchable' =>  false, 'data' =>  null, 'defaultContent' =>  ''],
        'id' => ['data' => 'id', 'name' => 'id'],
        'type' => ['data' => 'question_type', 'name' => 'question_type'],
        'question' => ['data' => 'content', 'name' => 'content'],
        'options' => ['data' => 'options', 'name' => 'options', 'orderable' => false, 'searcheable' => false]
    ];

    $table_cols_detail = [
        ['data' => 'id', 'name' => 'id'],
        ['data' => 'text', 'name' => 'text'],
        ['data' => 'img', 'name' => 'img', 'searchable' => false, 'orderable' => false],
        ['data' => 'options', 'name' => 'options', 'searchable' => false, 'orderable' => false],
        ['data' => 'is_correct', 'name' => 'is_correct']
    ];
@endphp
@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-sm-12">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Listado Preguntas</li>
                </ol>
            </nav>
        </div>
        <div class="float-right">
            <a href="{{ route('questions.create') }}" class="btn btn-primary" role="button"><i class="fas fa-plus-square"></i>&nbsp;&nbsp;Agregar Pregunta</a>
        </div>
        <br>
        <br>
        <div class="col-sm-12">
            <table id="table"  class="table table-striped table-bordered" style="width:100%" data-url="{{ route('datatable.questions') }}">
                <thead>
                    <tr>
                        <th data-column='@json($table_cols['id'])'>id</th>
                        <th data-column='@json($table_cols['control'])'></th>
                        <th data-column='@json($table_cols['question'])'>Pregunta</th>
                        <th data-column='@json($table_cols['type'])'>Tipo</th>
                        <th data-column='@json($table_cols['options'])'>Opciones</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        window.detail_columns = @json($table_cols_detail);
    </script>
    <script src="{{ mix('js/detail.js') }}"></script>
@endpush
