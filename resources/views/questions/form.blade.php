@php
    $form_route = isset($question) ? route('questions.update', $question->id) : route('questions.store');
    $table_cols = [
        'id' => ['data' => 'id', 'name' => 'id'],
        'text' => ['data' => 'text', 'name' => 'text'],
        'img' => ['data' => 'img', 'name' => 'img', 'searchable' => false, 'orderable' => false],
        'options' => ['data' => 'options', 'name' => 'options', 'searchable' => false, 'orderable' => false],
        'is_correct' => ['data' => 'is_correct', 'name' => 'is_correct']
    ];
@endphp

@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-sm-12">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('questions.index') }}">Listado Preguntas</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Pregunta</li>
                </ol>
            </nav>
        </div>
        @if(session()->has('saved') && session('saved'))
            <div class="col-sm-12">
                <div class="alert alert-success" role="alert">
                    Datos guardados correctamente.
                </div>
            </div>
        @elseif(session()->has('saved') && !session('saved'))
            <div class="col-sm-12">
                <div class="alert alert-danger" role="alert">
                    Hubo un problema al guardar.
                </div>
            </div>
        @endif
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">Datos</div>

                <div class="card-body">
                    <form action="{{ $form_route }}" method="POST">
                        @isset($question)
                            @method('PUT')
                        @endisset
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="content">Pregunta</label>
                            <input type="text" class="form-control" name="content" value="{{ isset($question) ? $question->content : null }}">
                        </div>
                        <div class="form-group">
                            <label for="question_type">Tipo</label>
                            <select class="form-control" name="question_type" @isset($question) disabled @endisset>
                                <option value="opcion" {{ isset($question) && $question->question_type == 'opcion' ? 'selected' : '' }}>Opción</option>
                                <option value="abierta" {{ isset($question) && $question->question_type == 'abierta' ? 'selected' : '' }}>Abierta</option>
                            </select>
                        </div>
                        <button type="submit" class="btn btn-primary">Guardar</button>
                    </form>
                </div>
            </div>
        </div>
        <br>
        @if(isset($question) && $question->question_type == 'opcion')
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <div class="float-left">
                            Preguntas
                        </div>
                        <div class="float-right">
                            <a href="{{ route('questions.answers.create', ['question' => $question->id]) }}" class="btn btn-primary" role="button"><i class="fas fa-plus-square"></i>&nbsp;&nbsp;Agregar Respuesta</a>
                        </div>
                    </div>
                    <div class="card-body">
                        <table>
                            <table id="table"  class="table table-striped table-bordered" style="width:100%" data-url="{{ route('datatable.answers', $question->id) }}">
                                <thead>
                                <tr>
                                    <th data-column='@json($table_cols['id'])'>id</th>
                                    <th data-column='@json($table_cols['text'])'>texto</th>
                                    <th data-column='@json($table_cols['img'])'>imagen</th>
                                    <th data-column='@json($table_cols['is_correct'])'>¿Correcto?</th>
                                    <th data-column='@json($table_cols['options'])'>editar</th>
                                </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </table>
                    </div>
                </div>
            </div>
        @endisset
    </div>
@endsection
