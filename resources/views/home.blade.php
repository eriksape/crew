@php
    $table_cols = [
        'name' => ['data' => 'name', 'name' => 'name', 'orderable' => false,],
        'estado' => ['data' => 'estado', 'estado' => 'estado', 'orderable' => false,],
        'code' => ['data' => 'code', 'name' => 'code', 'orderable' => false,],
        'time' => ['data' => 'time', 'name' => 'time', 'orderable' => false,],
        'score' => ['data' => 'score', 'name' => 'score', 'orderable' => false,],
        'open_answer' => ['data' => 'open_answer', 'name' => 'open_answer', 'orderable' => false,],
        'lugar_compra' => ['data' => 'lugar_compra', 'name' => 'lugar_compra', 'orderable' => false],
        'nombre_estilista' => ['data' => 'nombre_estilista', 'name' => 'nombre_estilista', 'orderable' => false],
        'edad' => ['data' => 'edad', 'name' => 'edad', 'orderable' => false],
        'phone' => ['data' => 'phone', 'name' => 'phone', 'orderable' => false],
        'email' => ['data' => 'email', 'name' => 'email', 'orderable' => false],
    ];
@endphp
@extends('layouts.app')
@section('content')
<div class="">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    Dashboard
                    <a id="download-btn" class="btn btn-sm btn-danger float-right text-white">Descargar</a>
                </div>
                <div class="card-body">

                    <table id="table" class="table table-striped table-bordered" style="width:100%"  data-url="{{ route('datatable.users') }}" data-orderable="false">
                        <thead>
                            <tr>
                                <th data-column='@json($table_cols['name'])'>Nombre</th>
                                <th data-column='@json($table_cols['estado'])'>Estado</th>
                                <th data-column='@json($table_cols['code'])'>Código</th>
                                <th data-column='@json($table_cols['time'])'>Tiempo</th>
                                <th data-column='@json($table_cols['score'])'>Aciertos</th>
                                <th data-column='@json($table_cols['open_answer'])'>Respuesta</th>
                                {{--
                                <th data-column='@json($table_cols['lugar_compra'])'>Nombre</th>
                                <th data-column='@json($table_cols['nombre_estilista'])'>Estado</th>
                                <th data-column='@json($table_cols['edad'])'>Código</th>
                                <th data-column='@json($table_cols['phone'])'>Tiempo</th>
                                <th data-column='@json($table_cols['email'])'>Aciertos</th>
                                --}}
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
