<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable = yes' name='viewport'>
    <meta content="{{route('bases')}}" name="quiz-rules">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="api-token" content="{{ Auth::user()->api_token }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <!-- Styles -->
    <link href="{{ asset('fonts/stylesheet.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <style>
        html, body {
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 12px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }

        .myImg {
            background-image: url("{{ asset('img/L1120511-2.png') }}");
            background-size: cover;
        }

        .app-image {
            background: linear-gradient(
                rgba(0, 0, 0, 0.78),
                rgba(0, 0, 0, 0.78)
            ), url({{asset('img/backgroundquiz.jpg')}});
            background-size: cover;
        }
        .app-image::after {
            content: "";

        }
    </style>
    <!-- Hotjar Tracking Code for http://stylefortheroad.com -->
    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:1029318,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>
</head>
<body>
<div class="container-fluid position-ref full-height app-image form-col">
    <div class="row align-self-start">
        <div class="col" style="padding: 0 64px; margin-top:48px">
            <div class="row">
                <div class="col-sm">
                    <img src="{{ asset('img/logoleft.png') }}" class="img-fluid" alt="logo">
                </div>
                <div class="col-sm text-right">
                    <img src="{{ asset('img/logoright4.png') }}" class="img-fluid" alt="logo">
                </div>
            </div>
        </div>
    </div>
    <div class="row align-items-center">
        <div class="col">
            <div id="example"></div>
        </div>
    </div>
</div>
<script src="{{ mix('js/quiz.js') }}"></script>
</body>
</html>
