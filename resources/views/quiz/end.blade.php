<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 12px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }

        .myImg {
            background-image: url("{{ asset('img/L1120511-2.png') }}");
            background-size: cover;
        }
    </style>
    <link href="{{ asset('fonts/stylesheet.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <!-- Hotjar Tracking Code for http://stylefortheroad.com -->
    <script>
      (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:1029318,hjsv:6};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
      })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>
</head>
<body style="background: #000;">
<div class="d-none d-lg-block d-xl-block">
    <div class="container-fluid position-ref full-height">
        <div class="row">
            <div class="col-sm-5 full-height myImg">
            </div>
            <div class="col-sm-7 full-height form-col" style="padding: 64px">
                <div class="row">
                    <div class="col">
                        <img src="{{ asset('img/logoleft.png') }}" class="img-fluid" alt="logo">
                    </div>
                    <div class="col">
                        <img src="{{ asset('img/logoright4.png') }}" class="img-fluid" alt="logo">
                    </div>
                </div>
                <div class="row text-center" id="footer-text">
                    <div class="col-sm-12">&nbsp;</div>
                    <div class="col-sm-12">
                        <a href="https://www.facebook.com/americancrewmexico" class="text-white"><i style="font-size: 2rem" class="fab fa-facebook-square"></i></a>
                    </div>
                    <div class="col-sm-12">&nbsp;</div>
                </div>
            </div>
        </div>
    </div>

</div>
{{--medium--}}
<div class="d-none d-md-block d-lg-none">
    <div class="container-fluid position-ref full-height">
        <div class="row">
            <div class="col-sm-5 full-height myImg">
            </div>
            <div class="col-sm-7 full-height form-col" style="padding: 64px">
                <div class="row">
                    <div class="col">
                        <img src="{{ asset('img/logoleft.png') }}" class="img-fluid" alt="logo">
                    </div>
                    <div class="col">
                        <img src="{{ asset('img/logoright4.png') }}" class="img-fluid" alt="logo">
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">&nbsp;</div>
                    <div class="col-sm-12">&nbsp;</div>
                    <div class="col-sm-12">&nbsp;</div>
                    <div class="col-sm-12 text-center">
                        <a href="https://www.facebook.com/americancrewmexico" class="text-white"><i style="font-size: 2rem" class="fab fa-facebook-square"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{--small--}}
<div class="d-block d-sm-none d-none d-sm-block d-md-none full-height form-col" style="padding: 32px">
    <div class="row">
        <div class="col" style="margin-bottom: 16px">
            <img src="{{ asset('img/logoleft.png') }}" class="img-fluid" alt="logo">
        </div>
        <div class="col" style="margin-bottom: 16px">
            <img src="{{ asset('img/logoright4.png') }}" class="img-fluid" alt="logo">
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">&nbsp;</div>
        <div class="col-sm-12 text-center">
            <a href="https://www.facebook.com/americancrewmexico" class="text-white"><i style="font-size: 2rem" class="fab fa-facebook-square"></i></a>
        </div>
    </div>
</div>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script
        src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha256-3edrmyuQ0w65f8gfBsqowzjJe2iM6n0nKciPUp8y+7E="
        crossorigin="anonymous"></script>
<script>
  $(function(){
    window.setTimeout(function(){
        window.location.replace("https://www.facebook.com/americancrewmexico");
    },500);
  });
</script>
<script src="{{ mix('js/microseconds.js') }}"></script>
<script src="{{ mix('js/register.js') }}"></script>
</body>
</html>

