<html><head><meta content="text/html; charset=UTF-8" http-equiv="content-type">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
            overflow: scroll;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 12px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }

        .myImg {
            background:
                linear-gradient(
                    rgba(0, 0, 0, 0.85),
                    rgba(0, 0, 0, 0.61)
                ),
                url({{ asset('img/L1120511-2.png') }});
            background-size: cover;
            background-repeat: repeat-y;
        }
        p, span, li {
            color: white !important;
        }
        td {
            border-color: white !important;
        }
    </style>
</head><body>
<div class="container-fluid position-ref full-height myImg">
    <div class="row">
        <div class="col" style="padding: 0 64px; margin-top:48px">
            <div class="row">
                <div class="col-sm">
                    <img src="{{ asset('img/logoleft.png') }}" class="img-fluid" alt="logo">
                </div>
                <div class="col-sm text-right">
                    <img src="{{ asset('img/logoright4.png') }}" class="img-fluid" alt="logo">
                </div>
            </div>
        </div>
    </div>
    <div class="row" id="participantes">
    </div>
    <div class="row">
        <div class="col d-flex justify-content-center">
            <a class="btn btn-primary" style="border-color: #f25a08;background-color: #f25a08; letter-spacing: 2px;" href=" {{ route('index') }} ">Regresar</a>
        </div>
    </div>
    <div class="row"><div class="col">&nbsp;</div></div>
    <div class="row"><div class="col">&nbsp;</div></div>
    <div class="row"><div class="col">&nbsp;</div></div>
</div>
<script src="{{ mix('js/participantes.js') }}"></script>
</body>
</html>
