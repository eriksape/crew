<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 12px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }

        .myImg {
            background-image: url("{{ asset('img/L1120511-2.png') }}");
            background-size: cover;
        }
    </style>
    <link href="{{ asset('fonts/stylesheet.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <!-- Hotjar Tracking Code for http://stylefortheroad.com -->
    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:1029318,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>
</head>
<body style="background: #000;">
<div class="d-none d-lg-block d-xl-block">
    <div class="container-fluid position-ref full-height">
        <div class="row">
            <div class="col-sm-5 full-height myImg">
            </div>
            <div class="col-sm-7 full-height form-col" style="padding: 64px">
                <div class="row">
                    <div class="col">
                        <img src="{{ asset('img/logoleft.png') }}" class="img-fluid" alt="logo">
                    </div>
                    <div class="col">
                        <img src="{{ asset('img/logoright4.png') }}" class="img-fluid" alt="logo">
                    </div>
                </div>
                <div class="row" style="margin-top: 16px">
                    <div class="col-sm-12 text-center">
                        <a href="{{route('participantes')}}" class="btn btn-primary" style="border-color: #f25a08;background-color: #f25a08; letter-spacing: 2px;">Consulta <br/>salones/barberías participantes</a>
                    </div>
                    <div class="col-sm-12">&nbsp;</div>
                    @if($errors->any())
                        <div class="col-sm-12">
                            <div class="alert alert-danger" role="alert">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </div>
                        </div>
                    @endif
                    <div class="col-sm">
                        <form class="quiz-form" method="POST" action="{{ route('register') }}">
                            @csrf
                            <input type="hidden" name="latitude"/>
                            <input type="hidden" name="longitude"/>
                            <input type="hidden" name="microseconds"/>
                            <div class="form-group row">
                                <label for="name" class="col-sm-2 col-form-label">Nombre</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}">
                                </div>
                            </div>
                            <fieldset class="form-group">
                                <div class="row">
                                    <legend class="col-form-label col-sm-2 pt-0">Edad</legend>
                                    <div class="col-sm-4">
                                        <div class="form-group row checkgroup">
                                            <label class="form-check-label col-auto" for="gridRadios1">
                                                17 - 25
                                            </label>
                                            <div class="col text-right">
                                                <input class="form-check-input" type="radio" name="edad" value="17 - 25" {{ (old('edad')) !== null ? ( old('edad') === '17 - 25' ? 'checked' : '' ) : 'checked' }}>
                                            </div>
                                        </div>
                                        <div class="form-group row checkgroup">
                                            <label class="form-check-label col-auto">
                                                26 - 35
                                            </label>
                                            <div class="col text-right">
                                                <input class="form-check-input" type="radio" name="edad" value="26 - 35" {{ old('edad') === '26 - 35' ? 'checked' : ''  }}>
                                            </div>
                                        </div>
                                        <div class="form-group row checkgroup">
                                            <label class="form-check-label col-auto" for="gridRadios2">
                                                36 - 45
                                            </label>
                                            <div class="col text-right">
                                                <input class="form-check-input" type="radio" name="edad" value="36 - 45" {{ old('edad') === '36 - 45' ? 'checked' : ''  }}>
                                            </div>
                                        </div>
                                        <div class="form-group row checkgroup">
                                            <label class="form-check-label col-auto">
                                                46 o MÁS
                                            </label>
                                            <div class="col text-right">
                                                <input class="form-check-input" type="radio" name="edad" value=">= 46" {{ old('edad') === '>= 46' ? 'checked' : ''  }}>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group row">
                                            <label for="email" class="col-auto col-form-label">E-mail</label>
                                            <div class="col">
                                                <input type="email" class="form-control" id="email" name="email" value="{{ old('email') }}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="phone" class="col-auto col-form-label">Teléfono</label>
                                            <div class="col">
                                                <input type="text" class="form-control" id="phone" name="phone" value="{{ old('phone') }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            <div class="form-group row">
                                <label for="estado" class="col-auto col-form-label">Estado</label>
                                <div class="col">
                                    <select class="form-control" id="estado" name="estado">
                                        <option value="">Selecciona una opción</option>
                                        <option value="Aguascalientes" @if(old('estado') == 'Aguascalientes') selected @endif>Aguascalientes</option>
                                        <option value="Baja California" @if(old('estado') == 'Baja California') selected @endif>Baja California</option>
                                        <option value="Baja California Sur" @if(old('estado') == 'Baja California Sur') selected @endif>Baja California Sur</option>
                                        <option value="Campeche" @if(old('estado') == 'Campeche') selected @endif>Campeche</option>
                                        <option value="CDMX" @if(old('estado') == 'CDMX') selected @endif>CDMX</option>
                                        <option value="Chiapas" @if(old('estado') == 'Chiapas') selected @endif>Chiapas</option>
                                        <option value="Chihuahua" @if(old('estado') == 'Chihuahua') selected @endif>Chihuahua</option>
                                        <option value="Coahuila" @if(old('estado') == 'Coahuila') selected @endif>Coahuila</option>
                                        <option value="Colima" @if(old('estado') == 'Colima') selected @endif>Colima</option>
                                        <option value="Durango" @if(old('estado') == 'Durango') selected @endif>Durango</option>
                                        <option value="Estado de México" @if(old('estado') == 'Estado de México') selected @endif>Estado de México</option>
                                        <option value="Guanajuato" @if(old('estado') == 'Guanajuato') selected @endif>Guanajuato</option>
                                        <option value="Guerrero" @if(old('estado') == 'Guerrero') selected @endif>Guerrero</option>
                                        <option value="Hidalgo" @if(old('estado') == 'Hidalgo') selected @endif>Hidalgo</option>
                                        <option value="Jalisco" @if(old('estado') == 'Jalisco') selected @endif>Jalisco</option>
                                        <option value="Michoacán" @if(old('estado') == 'Michoacán') selected @endif>Michoacán</option>
                                        <option value="Morelos" @if(old('estado') == 'Morelos') selected @endif>Morelos</option>
                                        <option value="Nayarit" @if(old('estado') == 'Nayarit') selected @endif>Nayarit</option>
                                        <option value="Nuevo León" @if(old('estado') == 'Nuevo León') selected @endif>Nuevo León</option>
                                        <option value="Oaxaca" @if(old('estado') == 'Oaxaca') selected @endif>Oaxaca</option>
                                        <option value="Puebla" @if(old('estado') == 'Puebla') selected @endif>Puebla</option>
                                        <option value="Querétaro" @if(old('estado') == 'Querétaro') selected @endif>Querétaro</option>
                                        <option value="Quintana Roo" @if(old('estado') == 'Quintana Roo') selected @endif>Quintana Roo</option>
                                        <option value="San Luis Potosí" @if(old('estado') == 'San Luis Potosí') selected @endif>San Luis Potosí</option>
                                        <option value="Sinaloa" @if(old('estado') == 'Sinaloa') selected @endif>Sinaloa</option>
                                        <option value="Sonora" @if(old('estado') == 'Sonora') selected @endif>Sonora</option>
                                        <option value="Tabasco" @if(old('estado') == 'Tabasco') selected @endif>Tabasco</option>
                                        <option value="Tamaulipas" @if(old('estado') == 'Tamaulipas') selected @endif>Tamaulipas</option>
                                        <option value="Tlaxcala" @if(old('estado') == 'Tlaxcala') selected @endif>Tlaxcala</option>
                                        <option value="Veracruz" @if(old('estado') == 'Veracruz') selected @endif>Veracruz</option>
                                        <option value="Yucatán" @if(old('estado') == 'Yucatán') selected @endif>Yucatán</option>
                                        <option value="Zacatecas" @if(old('estado') == 'Zacatecas') selected @endif>Zacatecas</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="place" class="col-auto col-form-label">Salón donde compraste american crew</label>
                                <div class="col">
                                    <input type="text" class="form-control" id="lugar_compra" name="lugar_compra" value="{{ old('lugar_compra') }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="estilista" class="col-auto col-form-label">Nombre de tu estilista</label>
                                <div class="col">
                                    <input type="text" class="form-control" id="nombre_estilista" name="nombre_estilista" value="{{ old('nombre_estilista') }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="code" class="col-auto col-form-label">Ingresa tu código</label>
                                <div class="col-4">
                                    <input type="text" class="form-control data-hj-whitelist" id="code" name="code" value="{{ old('code') }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-12 flex-center">
                                    <button type="button" class="btn btn-primary" style="border-color: #f25a08;background-color: #f25a08; letter-spacing: 2px;">Iniciar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="row text-center" id="footer-text">
                    <div class="col-sm-12">
                        <a href="{{route('bases')}}" class="text-white">Consulta bases y condiciones</a>
                    </div>
                    <div class="col-sm-12">&nbsp;</div>
                    <div class="col-sm-12">
                        <a href="https://www.facebook.com/americancrewmexico" class="text-white"><i style="font-size: 2rem" class="fab fa-facebook-square"></i></a>
                    </div>
                    <div class="col-sm-12">&nbsp;</div>
                </div>
            </div>
        </div>
    </div>

</div>
{{--medium--}}
<div class="d-none d-md-block d-lg-none">
    <div class="container-fluid position-ref full-height">
        <div class="row">
            <div class="col-sm-5 full-height myImg">
            </div>
            <div class="col-sm-7 full-height form-col" style="padding: 64px">
                <div class="row">
                    <div class="col">
                        <img src="{{ asset('img/logoleft.png') }}" class="img-fluid" alt="logo">
                    </div>
                    <div class="col">
                        <img src="{{ asset('img/logoright4.png') }}" class="img-fluid" alt="logo">
                    </div>
                </div>
                <div class="row" style="margin-top: 16px">
                    <div class="col-sm-12 text-center">
                        <a href="{{route('participantes')}}" class="btn btn-primary" style="border-color: #f25a08;background-color: #f25a08; letter-spacing: 2px;">Consulta <br/>salones/barberías participantes</a>
                    </div>
                    <div class="col-sm-12">&nbsp;</div>
                    <div class="col-sm">
                        <form class="quiz-form" method="POST" action="{{ route('register') }}">
                            @csrf
                            <input type="hidden" name="latitude"/>
                            <input type="hidden" name="longitude"/>
                            <input type="hidden" name="microseconds"/>
                            <div class="form-group row">
                                <label for="name" class="col-sm-2 col-form-label">Nombre</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}">
                                </div>
                            </div>
                            <fieldset class="form-group">
                                <div class="row">
                                    <legend class="col-form-label col-sm-2 pt-0">Edad</legend>
                                    <div class="col-sm-8">
                                        <div class="form-group row checkgroup">
                                            <label class="form-check-label col-auto" for="gridRadios1">
                                                17 - 25
                                            </label>
                                            <div class="col text-right">
                                                <input class="form-check-input" type="radio" name="edad" value="17 - 25" {{ (old('edad')) !== null ? ( old('edad') === '17 - 25' ? 'checked' : '' ) : 'checked' }}>
                                            </div>
                                        </div>
                                        <div class="form-group row checkgroup">
                                            <label class="form-check-label col-auto">
                                                26 - 35
                                            </label>
                                            <div class="col text-right">
                                                <input class="form-check-input" type="radio" name="edad" value="26 - 35" {{ old('edad') === '26 - 35' ? 'checked' : ''  }}>
                                            </div>
                                        </div>
                                        <div class="form-group row checkgroup">
                                            <label class="form-check-label col-auto" for="gridRadios2">
                                                36 - 45
                                            </label>
                                            <div class="col text-right">
                                                <input class="form-check-input" type="radio" name="edad" value="36 - 45" {{ old('edad') === '36 - 45' ? 'checked' : ''  }}>
                                            </div>
                                        </div>
                                        <div class="form-group row checkgroup">
                                            <label class="form-check-label col-auto">
                                                46 o MÁS
                                            </label>
                                            <div class="col text-right">
                                                <input class="form-check-input" type="radio" name="edad" value=">= 46" {{ old('edad') === '>= 46' ? 'checked' : ''  }}>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            <div class="form-group row">
                                <label for="email" class="col-auto col-form-label">E-mail</label>
                                <div class="col">
                                    <input type="email" class="form-control" id="email" name="email" value="{{ old('email') }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="phone" class="col-auto col-form-label">Teléfono</label>
                                <div class="col">
                                    <input type="text" class="form-control" id="phone" name="phone" value="{{ old('phone') }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="estado" class="col-auto col-form-label">Estado</label>
                                <div class="col">
                                    <select class="form-control" id="estado" name="estado">
                                        <option value="">Selecciona una opción</option>
                                        <option value="Aguascalientes" @if(old('estado') == 'Aguascalientes') selected @endif>Aguascalientes</option>
                                        <option value="Baja California" @if(old('estado') == 'Baja California') selected @endif>Baja California</option>
                                        <option value="Baja California Sur" @if(old('estado') == 'Baja California Sur') selected @endif>Baja California Sur</option>
                                        <option value="Campeche" @if(old('estado') == 'Campeche') selected @endif>Campeche</option>
                                        <option value="CDMX" @if(old('estado') == 'CDMX') selected @endif>CDMX</option>
                                        <option value="Chiapas" @if(old('estado') == 'Chiapas') selected @endif>Chiapas</option>
                                        <option value="Chihuahua" @if(old('estado') == 'Chihuahua') selected @endif>Chihuahua</option>
                                        <option value="Coahuila" @if(old('estado') == 'Coahuila') selected @endif>Coahuila</option>
                                        <option value="Colima" @if(old('estado') == 'Colima') selected @endif>Colima</option>
                                        <option value="Durango" @if(old('estado') == 'Durango') selected @endif>Durango</option>
                                        <option value="Estado de México" @if(old('estado') == 'Estado de México') selected @endif>Estado de México</option>
                                        <option value="Guanajuato" @if(old('estado') == 'Guanajuato') selected @endif>Guanajuato</option>
                                        <option value="Guerrero" @if(old('estado') == 'Guerrero') selected @endif>Guerrero</option>
                                        <option value="Hidalgo" @if(old('estado') == 'Hidalgo') selected @endif>Hidalgo</option>
                                        <option value="Jalisco" @if(old('estado') == 'Jalisco') selected @endif>Jalisco</option>
                                        <option value="Michoacán" @if(old('estado') == 'Michoacán') selected @endif>Michoacán</option>
                                        <option value="Morelos" @if(old('estado') == 'Morelos') selected @endif>Morelos</option>
                                        <option value="Nayarit" @if(old('estado') == 'Nayarit') selected @endif>Nayarit</option>
                                        <option value="Nuevo León" @if(old('estado') == 'Nuevo León') selected @endif>Nuevo León</option>
                                        <option value="Oaxaca" @if(old('estado') == 'Oaxaca') selected @endif>Oaxaca</option>
                                        <option value="Puebla" @if(old('estado') == 'Puebla') selected @endif>Puebla</option>
                                        <option value="Querétaro" @if(old('estado') == 'Querétaro') selected @endif>Querétaro</option>
                                        <option value="Quintana Roo" @if(old('estado') == 'Quintana Roo') selected @endif>Quintana Roo</option>
                                        <option value="San Luis Potosí" @if(old('estado') == 'San Luis Potosí') selected @endif>San Luis Potosí</option>
                                        <option value="Sinaloa" @if(old('estado') == 'Sinaloa') selected @endif>Sinaloa</option>
                                        <option value="Sonora" @if(old('estado') == 'Sonora') selected @endif>Sonora</option>
                                        <option value="Tabasco" @if(old('estado') == 'Tabasco') selected @endif>Tabasco</option>
                                        <option value="Tamaulipas" @if(old('estado') == 'Tamaulipas') selected @endif>Tamaulipas</option>
                                        <option value="Tlaxcala" @if(old('estado') == 'Tlaxcala') selected @endif>Tlaxcala</option>
                                        <option value="Veracruz" @if(old('estado') == 'Veracruz') selected @endif>Veracruz</option>
                                        <option value="Yucatán" @if(old('estado') == 'Yucatán') selected @endif>Yucatán</option>
                                        <option value="Zacatecas" @if(old('estado') == 'Zacatecas') selected @endif>Zacatecas</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="place" class="col-auto col-form-label">Salón donde compraste american crew</label>
                                <div class="col">
                                    <input type="text" class="form-control" id="lugar_compra" name="lugar_compra" value="{{ old('lugar_compra') }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="estilista" class="col-auto col-form-label">Nombre de tu estilista</label>
                                <div class="col">
                                    <input type="text" class="form-control" id="nombre_estilista" name="nombre_estilista" value="{{ old('nombre_estilista') }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="code" class="col-auto col-form-label">Ingresa tu código</label>
                                <div class="col-4">
                                    <input type="text" class="form-control data-hj-whitelist" id="code" name="code" value="{{ old('code') }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-12 flex-center">
                                    <button type="button" class="btn btn-primary" style="border-color: #f25a08;background-color: #f25a08; letter-spacing: 2px;">Iniciar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">&nbsp;</div>
                    <div class="col-sm-12">&nbsp;</div>
                    <div class="col-sm-12 text-center">
                        <a href="{{route('bases')}}" class="text-white">Consulta bases y condiciones</a>
                    </div>
                    <div class="col-sm-12">&nbsp;</div>
                    <div class="col-sm-12 text-center">
                        <a href="https://www.facebook.com/americancrewmexico" class="text-white"><i style="font-size: 2rem" class="fab fa-facebook-square"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{--small--}}
<div class="d-block d-sm-none d-none d-sm-block d-md-none full-height form-col" style="padding: 32px">
    <div class="row">
        <div class="col" style="margin-bottom: 16px">
            <img src="{{ asset('img/logoleft.png') }}" class="img-fluid" alt="logo">
        </div>
        <div class="col" style="margin-bottom: 16px">
            <img src="{{ asset('img/logoright4.png') }}" class="img-fluid" alt="logo">
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 text-center">
            <a href="{{route('participantes')}}" class="btn btn-primary" style="border-color: #f25a08;background-color: #f25a08; letter-spacing: 2px;">Consulta <br/>salones/barberías participantes</a>
        </div>
        <div class="col">
            <form class="quiz-form" method="POST" action="{{ route('register') }}">
                @csrf
                <input type="hidden" name="latitude"/>
                <input type="hidden" name="longitude"/>
                <input type="hidden" name="microseconds"/>
                <div class="form-group">
                    <label for="name">Nombre</label>
                    <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}">
                </div>
                <fieldset class="form-group">
                    <div class="row">
                        <legend class="col-form-label col-sm-2 pt-0">Edad</legend>
                        <div class="col-sm-4">
                            <div class="form-group row checkgroup">
                                <label class="form-check-label col-auto" for="gridRadios1">
                                    17 - 25
                                </label>
                                <div class="col text-right">
                                    <input class="form-check-input" type="radio" name="edad" value="17 - 25" {{ (old('edad')) !== null ? ( old('edad') === '17 - 25' ? 'checked' : '' ) : 'checked' }}>
                                </div>
                            </div>
                            <div class="form-group row checkgroup">
                                <label class="form-check-label col-auto">
                                    26 - 35
                                </label>
                                <div class="col text-right">
                                    <input class="form-check-input" type="radio" name="edad" value="26 - 35" {{ old('edad') === '26 - 35' ? 'checked' : ''  }}>
                                </div>
                            </div>
                            <div class="form-group row checkgroup">
                                <label class="form-check-label col-auto" for="gridRadios2">
                                    36 - 45
                                </label>
                                <div class="col text-right">
                                    <input class="form-check-input" type="radio" name="edad" value="36 - 45" {{ old('edad') === '36 - 45' ? 'checked' : ''  }}>
                                </div>
                            </div>
                            <div class="form-group row checkgroup">
                                <label class="form-check-label col-auto">
                                    46 o MÁS
                                </label>
                                <div class="col text-right">
                                    <input class="form-check-input" type="radio" name="edad" value=">= 46" {{ old('edad') === '>= 46' ? 'checked' : ''  }}>
                                </div>
                            </div>
                        </div>
                    </div>
                </fieldset>
                <div class="form-group">
                    <label for="email">E-mail</label>
                    <input type="email" class="form-control" id="email" name="email" value="{{ old('email') }}">
                </div>
                <div class="form-group">
                    <label for="phone">Teléfono</label>
                    <input type="text" class="form-control" id="phone" name="phone" value="{{ old('phone') }}">
                </div>
                <div class="form-group">
                    <label for="estado" class="">Estado</label>
                    <select class="form-control" id="estado" name="estado">
                        <option value="">Selecciona una opción</option>
                        <option value="Aguascalientes" @if(old('estado') == 'Aguascalientes') selected @endif>Aguascalientes</option>
                        <option value="Baja California" @if(old('estado') == 'Baja California') selected @endif>Baja California</option>
                        <option value="Baja California Sur" @if(old('estado') == 'Baja California Sur') selected @endif>Baja California Sur</option>
                        <option value="Campeche" @if(old('estado') == 'Campeche') selected @endif>Campeche</option>
                        <option value="CDMX" @if(old('estado') == 'CDMX') selected @endif>CDMX</option>
                        <option value="Chiapas" @if(old('estado') == 'Chiapas') selected @endif>Chiapas</option>
                        <option value="Chihuahua" @if(old('estado') == 'Chihuahua') selected @endif>Chihuahua</option>
                        <option value="Coahuila" @if(old('estado') == 'Coahuila') selected @endif>Coahuila</option>
                        <option value="Colima" @if(old('estado') == 'Colima') selected @endif>Colima</option>
                        <option value="Durango" @if(old('estado') == 'Durango') selected @endif>Durango</option>
                        <option value="Estado de México" @if(old('estado') == 'Estado de México') selected @endif>Estado de México</option>
                        <option value="Guanajuato" @if(old('estado') == 'Guanajuato') selected @endif>Guanajuato</option>
                        <option value="Guerrero" @if(old('estado') == 'Guerrero') selected @endif>Guerrero</option>
                        <option value="Hidalgo" @if(old('estado') == 'Hidalgo') selected @endif>Hidalgo</option>
                        <option value="Jalisco" @if(old('estado') == 'Jalisco') selected @endif>Jalisco</option>
                        <option value="Michoacán" @if(old('estado') == 'Michoacán') selected @endif>Michoacán</option>
                        <option value="Morelos" @if(old('estado') == 'Morelos') selected @endif>Morelos</option>
                        <option value="Nayarit" @if(old('estado') == 'Nayarit') selected @endif>Nayarit</option>
                        <option value="Nuevo León" @if(old('estado') == 'Nuevo León') selected @endif>Nuevo León</option>
                        <option value="Oaxaca" @if(old('estado') == 'Oaxaca') selected @endif>Oaxaca</option>
                        <option value="Puebla" @if(old('estado') == 'Puebla') selected @endif>Puebla</option>
                        <option value="Querétaro" @if(old('estado') == 'Querétaro') selected @endif>Querétaro</option>
                        <option value="Quintana Roo" @if(old('estado') == 'Quintana Roo') selected @endif>Quintana Roo</option>
                        <option value="San Luis Potosí" @if(old('estado') == 'San Luis Potosí') selected @endif>San Luis Potosí</option>
                        <option value="Sinaloa" @if(old('estado') == 'Sinaloa') selected @endif>Sinaloa</option>
                        <option value="Sonora" @if(old('estado') == 'Sonora') selected @endif>Sonora</option>
                        <option value="Tabasco" @if(old('estado') == 'Tabasco') selected @endif>Tabasco</option>
                        <option value="Tamaulipas" @if(old('estado') == 'Tamaulipas') selected @endif>Tamaulipas</option>
                        <option value="Tlaxcala" @if(old('estado') == 'Tlaxcala') selected @endif>Tlaxcala</option>
                        <option value="Veracruz" @if(old('estado') == 'Veracruz') selected @endif>Veracruz</option>
                        <option value="Yucatán" @if(old('estado') == 'Yucatán') selected @endif>Yucatán</option>
                        <option value="Zacatecas" @if(old('estado') == 'Zacatecas') selected @endif>Zacatecas</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="lugar_compra">Salón donde compraste american crew</label>
                    <input type="text" class="form-control" id="lugar_compra" name="lugar_compra" value="{{ old('lugar_compra') }}">
                </div>
                <div class="form-group">
                    <label for="nombre_estilista">Nombre de tu estilista</label>
                    <input type="text" class="form-control" id="nombre_estilista" name="nombre_estilista" value="{{ old('nombre_estilista') }}">
                </div>
                <div class="form-group">
                    <label for="code">Ingresa tu código</label>
                        <input type="text" class="form-control data-hj-whitelist" id="code" name="code" value="{{ old('code') }}">
                </div>
                <div class="form-group row">
                    <div class="col-sm-12 flex-center">
                        <button type="button" class="btn btn-primary" style="border-color: #f25a08;background-color: #f25a08; letter-spacing: 2px;">Iniciar</button>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">&nbsp;</div>
                    <div class="col-sm-12 text-center">
                        <a href="{{route('bases')}}" class="text-white">Consulta bases y condiciones</a>
                    </div>
                    <div class="col-sm-12">&nbsp;</div>
                    <div class="col-sm-12 text-center">
                        <a href="https://www.facebook.com/americancrewmexico" class="text-white"><i style="font-size: 2rem" class="fab fa-facebook-square"></i></a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script
    src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
    integrity="sha256-3edrmyuQ0w65f8gfBsqowzjJe2iM6n0nKciPUp8y+7E="
    crossorigin="anonymous"></script>
<script>
    $(function(){
        //var height = $('.col-sm-7.full-height').height() - $($('.col-sm-7.full-height .row')[1]).height() - $($('.col-sm-7.full-height .row')[0]).height();
        //$('#footer-text').css('margin-top', height/1.7);
    });
</script>
<script src="{{ mix('js/microseconds.js') }}"></script>
<script src="{{ mix('js/register.js') }}"></script>
</body>
</html>
