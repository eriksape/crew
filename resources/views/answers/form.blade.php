@php
    $form_route = isset($answer) ? route('questions.answers.update', ['question' => $question->id, 'answer' => $answer]) : route('questions.answers.store', $question->id);
@endphp

@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-sm-12">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('questions.index') }}">Listado Preguntas</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('questions.edit', $question) }}">Pregunta</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Respuesta</li>
                </ol>
            </nav>
        </div>
        @if(session()->has('saved') && session('saved'))
            <div class="col-sm-12">
                <div class="alert alert-success" role="alert">
                    Datos guardados correctamente.
                </div>
            </div>
        @elseif(session()->has('saved') && !session('saved'))
            <div class="col-sm-12">
                <div class="alert alert-danger" role="alert">
                    Hubo un problema al guardar.
                </div>
            </div>
        @endif
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">Datos</div>

                <div class="card-body">
                    <form action="{{ $form_route }}" method="POST" enctype="multipart/form-data">
                        @isset($answer)
                            @method('PUT')
                            <img src="{{ asset("app/$answer->image") }}">
                        @endisset
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="content_texto">Imagen</label>
                            <input type="text" name="text" class="form-control" value="{{isset($answer) ? $answer->text : null}}">
                        </div>
                        <div class="form-group">
                            <label for="content_imagen">Texto</label>
                            <input type="file" name="image" class="form-control-file">
                        </div>
                        <div class="form-check">
                            <input type="hidden" name="is_correct" value="0">
                            <input type="checkbox" class="form-check-input" name="is_correct" value="1" @isset($answer) @if($answer->is_correct) checked @endif @endisset>
                            <label class="form-check-label" for="is_correct">¿Respuesta Correcta?</label>
                        </div>
                        <button type="submit" class="btn btn-primary">Guardar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
