$(function(){
    let position = null;
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(({coords}) => position = coords);
    }
    $('form.quiz-form').on('submit', function(e, submit = false){
        if(submit === false) e.preventDefault();
    });

    $('button[type="button"]').on('click', function(e){
        return swal({
            title: "Reglas",
            text: "Una vez registrado iniciará el quiz.\nSolo se puede usar el código una vez.",
            icon: "warning",
            buttons: {
                cancel: "Cancelar",
                catch: {
                    text: "Iniciar",
                    value: "start",
                },
            }
        })
            .then((value) => {
                if(value === 'start'){
                    if(position !== null){
                        $(e.currentTarget).closest('form').find('input[name="latitude"]').val(position.latitude);
                        $(e.currentTarget).closest('form').find('input[name="longitude"]').val(position.longitude);
                    }
                    $(e.currentTarget).closest('form').find('input[name="microseconds"]').val(microseconds.now());
                    $(e.currentTarget).closest('form').trigger('submit', [true])
                }
            });
    });
});
