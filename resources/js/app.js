import xlsx from 'xlsx';

/**
 * First we will load all of this project's JavaScript dependencies which
 * includes React and other helpers. It's a great starting point while
 * building robust, powerful web applications using React + Laravel.
 */

require('./bootstrap');

(function($){
    const url = $('#table').data('url');
    const ordering = _.isUndefined($('#table').data('orderable')) ? true : $('#table').data('orderable');
    var columns = [];
    $('#table').find('th').each(function(){
        var column = $(this).data('column');
        columns.push(column);
    });

    $('#table').DataTable({
        dom: 'T<"clear">lfrtip',
        serverSide: true,
        processing: true,
        ordering,
        ajax : {
            url
        },
        columns,
        language: {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    });

})(window.$);

$(function(){
    $('#download-btn').on('click', function(){
        var api = $('#table').dataTable().api();
        const data =  api.ajax.params();
        data.action = 'excel';
        data.length = api.ajax.json().recordsTotal;
        data.start = 0;


        jQuery.get(  api.ajax.url(), data, function(result){
            const ws = xlsx.utils.json_to_sheet(result.data);

            const wb = xlsx.utils.book_new();
            xlsx.utils.book_append_sheet(wb, ws, "sheet1");
            xlsx.writeFile(wb, `users-style-for-theroad.xlsx`);
        } )
    });
})
