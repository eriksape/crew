setTimeout(function(){
    (function($){
        const template = function(id){
            return `<div class="label label-info">Detalle</div>`+
                `<table class="table details-table" id="${id}">`+
                `<thead><tr><th>id</th><th>texto</th><th>imagen</th><th>¿Correcto?</th><th>editar</th></tr></thead></table>`;
        };

        $('#table tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row =  $('#table').dataTable().api().row(tr);
            var tableId = `question-${row.data().id}`;

            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            } else {
                // Open this row
                row.child(template(tableId)).show();
                tr.addClass('shown');
                tr.next().find('td').addClass('no-padding bg-gray');
                initTable(tableId, row.data());
            }
        });

        function initTable(tableId, data) {
            $('#' + tableId).DataTable({
                dom: 'T<"clear">lfrtip',
                serverSide: true,
                processing: true,
                ajax: {
                    url : data.details_url
                },
                columns: window.detail_columns,
                language: {
                    "sProcessing":     "Procesando...",
                    "sLengthMenu":     "Mostrar _MENU_ registros",
                    "sZeroRecords":    "No se encontraron resultados",
                    "sEmptyTable":     "Ningún dato disponible en esta tabla",
                    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar:",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":     "Último",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
            })
        }
    })($);

}, 1000);
