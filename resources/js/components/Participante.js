import React, { Component, Fragment } from 'react';
import ReactDOM from 'react-dom';
import lodash from 'lodash';

export default class Participante extends Component {
    constructor(props){
        super(props);
        this.data = [
            {
                "direccion": "AV. SONORA 86, COL. ROMA NORTE, DEL. CUAHUTEMOC, CDMX",
                "municipio": "CUAUHTEMOC",
                "estado": "CDMX",
                "telefono": 5552114605,
                "salon": "CADEMIA PUPY"
            },
            {
                "direccion": "AV. COYOACAN ESQUINA DIVISION DEL NORTE, COL. DEL VALLE, BENITO JUAREZ",
                "municipio": "BENITO JUÁREZ",
                "estado": "CDMX",
                "telefono": 55438984,
                "salon": "TIENDA KOKORO"
            },
            {
                "direccion": "PIÑON 151, COL. NUEVA SANTA MARIA, AZCAPOTZALCO, CDMX",
                "municipio": "AZCAPOTZALCO",
                "estado": "CDMX",
                "telefono": 40000110,
                "salon": "SALÓN STILISIMO"
            },
            {
                "direccion": "AV UNIVERSIDAD 638, COL. LETRAN VALLE, DEL. BENITO JUAREZ",
                "municipio": "BENITO JUÁREZ",
                "estado": "CDMX",
                "telefono": "56167222 / 5564317317",
                "salon": "SALON CETOUT"
            },
            {
                "direccion": "AV. GABRIEL MANCERA 1063, LOCAL 1 COL. DEL VALLE CENTRO, BENITO JUAREZ, CDMX",
                "municipio": "BENITO JUÁREZ",
                "estado": "CDMX",
                "telefono": 67945804,
                "salon": "ESTETICA STOA BEAUTY CENTER"
            },
            {
                "direccion": "DAKOTA 95 LOCAL 7, COL. NAPOLES, BENITO JUAREZ, CDMX",
                "municipio": "BENITO JUÁREZ",
                "estado": "CDMX",
                "telefono": 96273522,
                "salon": "ESTETICA STOA BEAUTY CENTER"
            },
            {
                "direccion": "AV. ACOXPA 430 LOCAL 4 Y 5, COL. EX HACIENDA COAPA, DEL. TLAPAN",
                "municipio": "TLALPAN",
                "estado": "CDMX",
                "telefono": 56780196,
                "salon": "ESTETICA STOA BEAUTY CENTER"
            },
            {
                "direccion": "AV. UNIVERSIDAD 1778, LOCAL ML03, COL. ROMERO DE TERREROS, DEL. COYOACAN, CMDX",
                "municipio": "COYOACAN",
                "estado": "CDMX",
                "telefono": 91548956,
                "salon": "OASIS"
            },
            {
                "direccion": "AV. CANAL DE TEZONTLE 1512 LOCAL 277, COL. ROJO GOMEZ, DEL. IZTAPALAPA",
                "municipio": "IZTAPALAPA",
                "estado": "CDMX",
                "telefono": 91290064,
                "salon": "BARBER SHOP STOA"
            },
            {
                "direccion": "AV. SAN FERNANDO 649 LOCAL 8, COL. PEÑA POBRE, DEL. TLALPAN, CIUDAD DE  MEXICO",
                "municipio": "TLALPAN",
                "estado": "CDMX",
                "telefono": 54244037,
                "salon": "BOXER BARBER SHOP"
            },
            {
                "direccion": "AV ADOLFO LOPEZ MATEOS 4020 SECCION, B 1-A, COL. JARDINES DEL PEDREGAL, COYOACAN, MEXICO",
                "municipio": "COYOACAN",
                "estado": "CDMX",
                "telefono": 51352231,
                "salon": "BELLEZA SA DE CV"
            },
            {
                "direccion": "AV REVOLUCION 750, COL. NONOALCO, DEL.  BENITO JUAREZ",
                "municipio": "BENITO JUÁREZ",
                "estado": "CDMX",
                "telefono": 68125522,
                "salon": "DCMAAT SA DE CV"
            },
            {
                "direccion": "CALZADA EL HUESO 503,  COL. LOS GIRASOLES, COYOACAN, MEXICO",
                "municipio": "COYOACAN",
                "estado": "CDMX",
                "telefono": 56797164,
                "salon": "NEW STYLE"
            },
            {
                "direccion": "MIGUEL ANGEL DE QUEVEDO 287 LOCAL 19, COL. ROMERO DE TERREROS, DEL. COYOACAN",
                "municipio": "COYOACAN",
                "estado": "CDMX",
                "telefono": 5564216488,
                "salon": "PELUQUERIA BOXERS"
            },
            {
                "direccion": "AV  418 FRANCISCO MORAZAN 888, LOCAL 1 COL. VILLA DE ARAGON, GUSTAVO A MADERO, CIUDAD DE  MEXICO",
                "municipio": "GUSTAVO A. MADERO",
                "estado": "CDMX",
                "telefono": "5521433529 / 72580595 / 72580591",
                "salon": "THE PADRINOS  BARBER SHOP"
            },
            {
                "direccion": "AVENIDA 8 226 A, COL. IGNACIO ZARAGOZA, VENUSTIANO CARRANZA, MEXICO, D.F.",
                "municipio": "VENUSTIANO CARRAZNA",
                "estado": "CDMX",
                "telefono": "68406353 y 55-30512545",
                "salon": "SEBASTIÁN GAGHER SALÓN MAKE UP STUDIO"
            },
            {
                "direccion": "AVENIDA PATRIA 240, INT-7 COL. LA ESTANCIA, ZAPOPAN, JAL",
                "municipio": "ZAPOPAN",
                "estado": "JALISCO",
                "telefono": "01-33-36821295",
                "salon": "LOCCOCO LA  ESTANCIA"
            },
            {
                "direccion": "CALLE HELIOS 1186, COL. MIRADOR DEL SOL, ZAPOPAN, JALISCO",
                "municipio": "ZAPOPAN",
                "estado": "JALISCO",
                "telefono": "01-33-31211145",
                "salon": "BERNI SALON"
            },
            {
                "direccion": "COLONIAS 221 LL-08-PB, COL. AMERICANA, GUADALAJARA, JALISCO",
                "municipio": "GUADALAJARA",
                "estado": "JALISCO",
                "telefono": "01-33-38092892",
                "salon": "BARBER SHOP LOCCOCO"
            },
            {
                "direccion": "SEVILLA 1174,  COL. ALCALDE BARRANQUITAS, GUADALAJARA, JAL",
                "municipio": "GUADALAJARA",
                "estado": "JALISCO",
                "telefono": 17328561,
                "salon": "ALEJANDRO&MANCERA"
            },
            {
                "direccion": "AV. MEXICO 3300,  COL. MONRAZ, GUADALAJARA, JAL",
                "municipio": "GUADALAJARA",
                "estado": "JALISCO",
                "telefono": "01-333-8132661",
                "salon": "SELENE HAIR STUDIO"
            },
            {
                "direccion": "AV . LOPEZ MATEOS SUR 5565,  COL. JARDINES DE SANTA ANITA, TLAJOMULCO DE ZUÑIGA, TLAJOMULCO DE ZUÑIGA, JAL",
                "municipio": "TLAJOMULCO",
                "estado": "JALISCO",
                "telefono": "01-333-6864255",
                "salon": "LOCCOCOCO STA ANITA"
            },
            {
                "direccion": "AVENIDA VALLARTA 1940, COL. OBRERA CENTRO, GUADALAJARA, JAL",
                "municipio": "GUADALAJARA",
                "estado": "JALISCO",
                "telefono": "01-333-6155358",
                "salon": "RAMIREZ MUÑOZ HECTOR"
            },
            {
                "direccion": "RUBEN DARIO 458 1, COL. CIRCUNVALACION GUEVARA, GUADALAJARA, JALISCO",
                "municipio": "GUADALAJARA",
                "estado": "JALISCO",
                "telefono": 36247687,
                "salon": "CARIS STUDIO"
            },
            {
                "direccion": "AMSTERDAM 275-PB, COL. HIPODROMO CONDESA, CUAUHTEMOC, CMDX",
                "municipio": "CUAUHTEMOC",
                "estado": "CDMX",
                "telefono": 5552643481,
                "salon": "SALON  CONTRASTES"
            },
            {
                "direccion": "EJE 7 SUR 47-A, INSURGENTES MIXCOAC , BENITO JUÁREZ, 03920",
                "municipio": "BENITO JUÁREZ",
                "estado": "CDMX",
                "telefono": 5556153339,
                "salon": "ICON SALÓN"
            },
            {
                "direccion": "BAHIA DEL ESPITITU SANTO 43 LOC 3, COL. ANAHUAC, MIGUEL HIDALGO, CDMX",
                "municipio": "MIGUEL HIDALGO",
                "estado": "CDMX",
                "telefono": 5552600666,
                "salon": "KAPRICHOS"
            },
            {
                "direccion": "CALLE COL. DEL VALLE 504 LOC 1, COL. DEL VALLE, BENITO JUAREZ, MEXICO DF",
                "municipio": "BENITO JUÁREZ",
                "estado": "CDMX",
                "telefono": 55239124,
                "salon": "LA BARBERÍA SHOP & SPA MÉXICO"
            },
            {
                "direccion": "ORIZABA 101 C, COL. ROMA, CUAUHTEMOC, MEXICO",
                "municipio": "CUAUHTEMOC",
                "estado": "CDMX",
                "telefono": 5555143458,
                "salon": "VALDESPINO HAIRSTYLE"
            },
            {
                "direccion": "CELAYA 9, COL. CONDESA, CUAUHTEMOC, MEXICO DF",
                "municipio": "CUAUHTEMOC",
                "estado": "CDMX",
                "telefono": 70984188,
                "salon": "BARBERÍA HIPÓDROMO"
            },
            {
                "direccion": "PATRIOTISMO 229 SOTANO 1 LOCAL E 11, COL. SAN PEDRO DE  LOS PINOS, BENITO JUAREZ, MEXICO",
                "municipio": "BENITO JUÁREZ",
                "estado": "CDMX",
                "telefono": 5591307391,
                "salon": "CAMDEN"
            },
            {
                "direccion": "JORGE WASHINGTON 145 LOC 2, COL. MODERNA, BENITO JUAREZ, MEXICO",
                "municipio": "BENITO JUÁREZ",
                "estado": "CDMX",
                "telefono": 65836855,
                "salon": "MAX TORRADO"
            },
            {
                "direccion": "EDGAR ALLAN POE 94 PH, COL. POLANCO REFORMA, DEL. MIGUEL HIDALGO, CDMX.",
                "municipio": "MIGUEL HIDALGO",
                "estado": "CDMX",
                "telefono": 52807977,
                "salon": "THE BARBER SHOP REVE"
            },
            {
                "direccion": "AV. DE LOS BOSQUES 234, COL. LOMAS DE TECAMACHALCO, HUIXQUILUCAN, HUIXQUILUCAN",
                "municipio": "HUIXQUILUCAN",
                "estado": "EDO MEX",
                "telefono": 63038622,
                "salon": "PARIS 2000"
            },
            {
                "direccion": "JESUS DEL MONTE 37, LOCAL 13 COL. JESUS DEL MONTE, HUIXQUILUCAN, MEXICO",
                "municipio": "HUIXQUILUCAN",
                "estado": "EDO MEX",
                "telefono": 52470984,
                "salon": "SALON PARIS 2000"
            },
            {
                "direccion": "BOSQUES DE ARRAYAN MZ 5 LT 1, 414 Y 412 ,COL. FRACC  BOSQUE ESMERALDA, ATIZAPAN DE ZARAGOZA, EDOMEX",
                "municipio": "ATIZAPÁN DE ZARAGOZA",
                "estado": "EDO MEX",
                "telefono": 5518110671,
                "salon": "ESTETICA BY  DANIEL  CORREA"
            },
            {
                "direccion": "MONTE LIBANO 915 LOCAL O, COL. LOMAS DE CHAPULTEPEC, MIGUEL HIDALGO, CDMX",
                "municipio": "MIGUEL HIDALGO",
                "estado": "CDMX",
                "telefono": 52929601,
                "salon": "ESTETICA MASCULINA  PARIS  2000"
            },
            {
                "direccion": "PEDREGAL 71, COL. LOMAS DE CHAPULTEPEC I SECCION, MIGUEL HIDALGO, MEXICO",
                "municipio": "MIGUEL HIDALGO",
                "estado": "CDMX",
                "telefono": 55407096,
                "salon": "ORANGE BEAUTE MEXIQUE"
            },
            {
                "direccion": "VALLE DE MEXICO 28, COL. EL MIRADOR, NAUCALPAN DE JUAREZ, MEXICO",
                "municipio": "NAUCALPAN DE JUÁREZ",
                "estado": "EDO MEX",
                "telefono": 43331422,
                "salon": "BACKSTYLE SALON"
            },
            {
                "direccion": "TECNOLOGICO 1600, LOCAL 36 B COL. SAN SALVADOR TIZATLALLI, METEPEC, MEXICO",
                "municipio": "METEPEC",
                "estado": "EDO MEX",
                "telefono": 5567834798,
                "salon": "PARIS 2000 MASCULINA"
            },
            {
                "direccion": "AV. UNO A 21, COL. SANTA ROSA, GUSTAVO A. MADERO, CDMX",
                "municipio": "GUSTAVO A. MADERO",
                "estado": "CDMX",
                "telefono": 53881965,
                "salon": "SALÓN ILUSIÓN"
            },
            {
                "direccion": "AVENIDA DEL PARQUE 108, COL. PARQUE  RESIDENCIAL COACALCO, COACALCO DE BERRIOZABAL, MEXICO, D.F.",
                "municipio": "COACALCO",
                "estado": "EDO MEX",
                "telefono": 26003986,
                "salon": "SALON VIVA GLAM"
            },
            {
                "direccion": "MONTEVIDEO (EJE 5 NORTE) 363, LOCAL 307 COL. LINDAVISTA SUR, GUSTAVO A MADERO, MEXICO",
                "municipio": "GUSTAVO A. MADERO",
                "estado": "CDMX",
                "telefono": 55866584,
                "salon": "SALON SNOB"
            },
            {
                "direccion": "PRIVADA VENUSTIANO CARRANZA 5, COL. CUAUHTEPEC DE MADERO, GUSTAVO A. MADERO, MEXICO, D.F.",
                "municipio": "GUSTAVO A. MADERO",
                "estado": "CDMX",
                "telefono": 56038824,
                "salon": "SALON ARMANDO'S"
            },
            {
                "direccion": "AV CLAVERIA 94, LOC UNICO COL. CLAVERIA, AZCAPOTZALCO, MEXICO",
                "municipio": "AZCAPOTZALCO",
                "estado": "CDMX",
                "telefono": 53999998,
                "salon": "ESTILUS"
            },
            {
                "direccion": "VIA MORELOS 172, LOCAL  L COL. LOS LAURELES, ECATEPEC DE MORELOS, MEXICO",
                "municipio": "ECATEPEC",
                "estado": "EDO MEX",
                "telefono": 11154803,
                "salon": "SALON VISAGE"
            },
            {
                "direccion": "VIA MORELOS 172 LOCAL E, COL. FRACCIONAMIENTOS LOS LAURELES, ECATEPEC DE MORELOS, EDOMEX",
                "municipio": "ECATEPEC",
                "estado": "EDO MEX",
                "telefono": 11154803,
                "salon": "SALON VISAGE"
            },
            {
                "direccion": "JUAREZ NORTE 11, COL. SAN CRISTOBAL CENTRO, ECATEPEC DE MORELOS, EDO DE MEXICO",
                "municipio": "ECATEPEC",
                "estado": "EDO MEX",
                "telefono": 51166759,
                "salon": "KASSIANS"
            },
            {
                "direccion": "VALLE DE SAN JAVIER 407, COL. VALLE DE SAN JAVIER, PACHUCA DE SOTO, HIDALGO",
                "municipio": "PACHUCA",
                "estado": "HIDALGO",
                "telefono": "01-77-17104000",
                "salon": "NUOVAERA"
            },
            {
                "direccion": "MACRO PLAZA LOS HEROES LOC G-20, BOSQUES TECAMAC, EDOMEX",
                "municipio": "TECAMAC",
                "estado": "EDO MEX",
                "telefono": 10864082,
                "salon": "SALON AY L"
            },
            {
                "direccion": "AV. CIRCUITO BOSQUES DE BOLOGNIA 249, BOSQUES DEL LAGO, CAUTITLAN IZCALLI, EDO MEX",
                "municipio": "CAUTITLAN IZCALLI",
                "estado": "EDO MEX",
                "telefono": 5517841404,
                "salon": "ESTETICA MONTES SALON"
            },
            {
                "direccion": "NARANJO 21 LOC 3, COL. CUXTITLA, TIZAYUCA, ECATEPEC",
                "municipio": "TIZAYUCA",
                "estado": "EDO MEX",
                "telefono": "01-77-97961986",
                "salon": "SALON  VINTAGE"
            },
            {
                "direccion": "CALLE 303, 610, COL. NUEVA ATZACOALCO, GUSTAVO A MADERO, CDMX",
                "municipio": "GUSTAVO A. MADERO",
                "estado": "CDMX",
                "telefono": 57691867,
                "salon": "SAGITARIOS ESPECIALISTAS EN BELLEZA"
            },
            {
                "direccion": "PLAZA RIO DE LOS REMEDIOS : AV RIO DE LOS REMEDIOS 5 LOC 11, SAN JUANICO, TLANEPANTLA, EDO DE MEXICO",
                "municipio": "TLALNEPANTLA",
                "estado": "EDO MEX",
                "telefono": "16 610830",
                "salon": "SAGITARIOS ESPECIALISTAS EN BELLEZA"
            },
            {
                "direccion": "VALLARTA 2425, COL. ARCOS VALLARTA, GUADALAJARA, JAL.",
                "municipio": "GUADALAJARA",
                "estado": "JALISCO",
                "telefono": "01-33-36304108",
                "salon": "MARGARET MURE"
            },
            {
                "direccion": "AVENIDA VALLARTA 5566, LOCAL E-2, COL. LOMAS UNIVERSIDAD, ZAPOPAN, JAL",
                "municipio": "ZAPOPAN",
                "estado": "JALISCO",
                "telefono": "01-33-36829833",
                "salon": "LOCCOCO  LOMAS  UNIVERSIDAD"
            },
            {
                "direccion": "LUIS PEREZ VERDIA 61 A, COL. LADRON DE GUEVARA, GUADALAJARA, JALISCO",
                "municipio": "GUADALAJARA",
                "estado": "JALISCO",
                "telefono": "01-33-15932710",
                "salon": "SALA 21"
            },
            {
                "direccion": "AV. RAFAEL SANZIO 505 LOCAL 5, COL. RESIDENCIAL LA ESTANCIA, ZAPOPAN, JAL.",
                "municipio": "ZAPOPAN",
                "estado": "JALISCO",
                "telefono": "01-33-30440540",
                "salon": "MICHAEL ROMILL SALON"
            },
            {
                "direccion": "CALLE PUEBLA NORTE 54 F, COL. CENTRO, UNION DE TULA, JALISCO",
                "municipio": "UNION DE TULA",
                "estado": "JALISCO",
                "telefono": "01-31-71082282",
                "salon": "SALON 05"
            },
            {
                "direccion": "AVENIDA INDUSTRIA TEXTIL 1587, COL. VALLE DE SAN ISIDRO, ZAPOPAN, JALISCO",
                "municipio": "ZAPOPAN",
                "estado": "JALISCO",
                "telefono": "01-33-16004220",
                "salon": "VAED BEAUTY CENTER"
            },
            {
                "direccion": "AV MARIANO OTERO 5654-A, COL. PASEOS DEL SOL, ZAPOPAN, JALISCO",
                "municipio": "ZAPOPAN",
                "estado": "JALISCO",
                "telefono": "01-33-31336090",
                "salon": "QUTÉ ESTUDIO DE BELLEZA"
            },
            {
                "direccion": "ANGULO 1674, COL. SANTA TERESITA, GUADALAJARA, JALISCO",
                "municipio": "GUADALAJARA",
                "estado": "JALISCO",
                "telefono": "01-33-12569866",
                "salon": "PRIVEE STUDIO"
            },
            {
                "direccion": "AVENIDA VALLARTA 3959 LOCAL G-21, COL. JARDINES  DE LOS ARCOS, ZAPOPAN, JAL. \"GRAN PLAZA",
                "municipio": "ZAPOPAN",
                "estado": "JALISCO",
                "telefono": "01-33-31225218",
                "salon": "CRISTIAN SPA"
            },
            {
                "direccion": "RUBEN DARÍO 1435, COL. PROVIDENCIA, GUADALAJARA, JAL.",
                "municipio": "GUADALAJARA",
                "estado": "JALISCO",
                "telefono": "01-33-18174077",
                "salon": "BARBIERATTOO"
            },
            {
                "direccion": "33 PONIENTE 1505, COL. LOS VOLCANES, ENTRE 15 Y 17  SUR, PUEBLA, PUEBLA",
                "municipio": "PUEBLA",
                "estado": "PUEBLA",
                "telefono": "01-22-21144306",
                "salon": "ALEC MORE ALTA PELUQUERIA"
            },
            {
                "direccion": "ACACIAS MZ-2 LT-14 CASA-102, COL. FRACCIONAMIENTO SAUCES, TOLUCA, TOLUCA",
                "municipio": "TOLUCA",
                "estado": "EDO MEX",
                "telefono": "01-72-22102793",
                "salon": "ESTETICA MINAGE"
            },
            {
                "direccion": "LEONA VICARIO 801, L1 PA, COL. LA PURISIMA, METEPEC, MEXICO",
                "municipio": "METEPEC",
                "estado": "EDO MEX",
                "telefono": "01-72-22190510",
                "salon": "AGUILA LOURDES"
            },
            {
                "direccion": "XITLI 105, COL. XINANTECATL, METEPEC, METEPEC",
                "municipio": "METEPEC",
                "estado": "EDO MEX",
                "telefono": "01-72-22703968",
                "salon": "RIZZO"
            },
            {
                "direccion": "AV. IGNACIO COMONFORT 1506 PLANTA ALTA, COL. PROVIDENCIA, METEPEC, MEX",
                "municipio": "METEPEC",
                "estado": "EDO MEX",
                "telefono": "01-72-21802404",
                "salon": "EUFORIA"
            },
            {
                "direccion": "CALLE 38, 260 LOCAL 2, COL. CAMPESTRE, MERIDA, YUCATAN",
                "municipio": "MERIDA",
                "estado": "YUCATÁN",
                "telefono": "01-99-93381841",
                "salon": "GENARO MTZ SALON"
            },
            {
                "direccion": "AV. VALLARTA 3959 LOCAL P 2DO PISO, COL. DON BOSCO VALLARTA, ZAPOPAN JAL. \"GRAN PLAZA",
                "municipio": "ZAPOPAN",
                "estado": "JALISCO",
                "telefono": "01-33-36211350",
                "salon": "BOXER BARBER SHOP"
            },
            {
                "direccion": "AV. GUADALUPE 6000, COL. PLAZA GUADALUPE, ZAPOPAN, JAL. \"PLAZA ALEGRA",
                "municipio": "ZAPOPAN",
                "estado": "JALISCO",
                "telefono": "01-33-36204780",
                "salon": "BOXER BARBER SHOP"
            }
        ];

        this.state = {
            estado : 'all' ,
            data: this.data,
            showMunicipio: false,
        };
    }
    changeEstado(event){
        this.setState({
            estado: event.currentTarget.value,
            data: event.currentTarget.value === 'all' ? this.data : this.data.filter( d => d.estado === event.currentTarget.value ),
            showMunicipio: true,
        });
    }
    changeMunicipio(event){
        this.setState({
            data: event.currentTarget.value === 'all' ? this.data.filter( d => d.estado === this.state.estado ) : this.data.filter( d => d.municipio === event.currentTarget.value && d.estado === this.state.estado ),
        });
    }
    render(){
        return(
            <Fragment>
                <div className="col-12">
                    <div className="form-group">
                        <label htmlFor="exampleFormControlSelect1">Selecciona Estado</label>
                        <select className="form-control" id="exampleFormControlSelect1" onChange={this.changeEstado.bind(this)}>
                            <option value="all">Selecciona un valor</option>
                            { _.chain(this.data).mapValues('estado').values().uniq().value()
                                .map(function(d, key){
                                return(
                                    <option key={key} value={ d }>{ d }</option>
                                )
                            }) }
                        </select>
                    </div>
                </div>
                {
                    this.state.showMunicipio ? (<div className="col-12">
                    <div className="form-group">
                        <label htmlFor="exampleFormControlSelect1">Selecciona Delegación o Municipio</label>
                        <select className="form-control" id="exampleFormControlSelect1" onChange={this.changeMunicipio.bind(this)}>
                            <option value="all">Selecciona un valor</option>
                            { _.chain(this.data).filter(d => d.estado === this.state.estado).mapValues('municipio').values().uniq().value()
                                .map(function(d, key){
                                    return(
                                        <option key={key} value={ d }>{ d }</option>
                                    )
                                }) }
                        </select>
                    </div>
                    </div>) : (<div className="col-12"></div>)
                }
                <div className="col d-flex justify-content-center">
                    <table className="table table-striped table-dark">
                        <thead className="thead-dark">
                        <tr>
                            <th title="Field #1">Estética o Salón</th>
                            <th title="Field #2">Dirección</th>
                            <th title="Field #3">Teléfono</th>
                            <th title="Field #4">Estado</th>
                            <th title="Field #5">Delegación o Municipio</th>
                        </tr>
                        </thead>
                        <tbody>
                        { this.state.data.map(function(d, key){
                            return(
                                <tr key={key}>
                                    <td>{ d.salon }</td>
                                    <td>{ d.direccion }</td>
                                    <td>{ d.telefono }</td>
                                    <td>{ d.estado }</td>
                                    <td>{ d.municipio }</td>
                                </tr>
                            )
                        }) }
                        </tbody>
                    </table>
                </div>
            </Fragment>
        );
    }
}

if (document.getElementById('participantes')) {
    ReactDOM.render(<Participante />, document.getElementById('participantes'));
}
