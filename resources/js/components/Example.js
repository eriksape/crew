import React, { Component, Fragment } from 'react';
import ReactDOM from 'react-dom';
import microseconds from 'microseconds';

export default class Example extends Component {
    constructor(props){
        super(props);
        this.state = {
            question: null
        };
        this.getQuestion();
    }
    getQuestion(){
        axios.post('/api/question').then( ({data}) => this.setState({question:data}) );
    }
    setAnswer(id){
        this.setState({question:null});
        let text = '';
        if(document.querySelector('textarea')) text = document.querySelector('textarea').value;

        axios.post('/api/answer', {
            answer: id,
            text:text,
            microseconds: microseconds.now(),
        }).then(()=>{
            this.getQuestion();
        });
    }
    exit(){
        axios.post('/logout').then(()=>{
           setTimeout(()=>{ document.location.href="https://www.facebook.com/americancrewmexico"; }, 2000)
        });
    }
    render() {
        if(this.state.question == null)
            return(
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col d-flex justify-content-center">
                            <Loader />
                        </div>
                    </div>
                </div>
            );
        if(this.state.question.question_type === 'opcion')
            return (
                <div className="container">
                    <div className="row justify-content-center text-center" style={{marginTop:12}}>
                        <div className="col">
                            <div className="quiz quiz-title">{ this.state.question.content }</div>
                        </div>
                    </div>
                    <div className="row" style={{marginTop:12, padding:this.state.question.answers.length === 2 ? 32:0}}>
                        { _.shuffle(this.state.question.answers).map(( answer, key )=>( <AnswerBox key={key} length={this.state.question.answers.length} answer={answer} onClick={()=>{this.setAnswer(answer.id)}}/> )) }
                    </div>
                </div>
            );
        if(this.state.question.question_type === 'abierta')
            return (
                <div className="container">
                    <div className="row justify-content-center text-center" style={{marginTop:12}}>
                        <div className="col">
                            <div className="quiz quiz-title">{ this.state.question.content }</div>
                        </div>
                    </div>
                    <div className="row" style={{marginTop:24}}>
                        <div className="col-sm-12">
                            <textarea className="form-control" style={{opacity: 0.68}} rows="6"></textarea>
                        </div>
                        <div className="col-sm-12 flex-center" style={{marginTop:24}}>
                            <button type="button" className="btn btn-primary quiz-form" onClick={()=>this.setAnswer(this.state.question.answers[0].id)}
                                    style={{borderColor: '#f25a08',backgroundColor:'#f25a08', letterSpacing: 2}}>
                                { this.state.question.last ? 'Finalizar' : 'Continuar' }
                            </button>
                        </div>
                    </div>
                </div>
            );

        let milliseconds = this.state.question.microseconds ? (microseconds.parse(parseInt(this.state.question.microseconds.end) - parseInt(this.state.question.microseconds.start)).milliseconds): false;
        return (
            <div className="container">
                <div className="row justify-content-center text-center d-none d-sm-block" style={{marginTop:12}}>
                    <div className="col-sm-3"></div>
                    <div className="col">
                        <img src='/img/finish.png' className="img-fluid"></img>
                    </div>
                    <div className="col-sm-3"></div>
                </div>
                <div className="row" style={{marginTop:32}}>
                    <div className="col-sm-12">
                        <p className="finish-text text-center data-hj-whitelist">¡FELICIDADES tu estilo ha llegado muy lejos!</p>
                    </div>
                </div>
                <div className="row" style={{marginTop:-32}}>
                    <div className="col-sm-12">
                        <p className="time-text text-center">{ this.state.question.time }{ milliseconds ? `.${milliseconds}` :''}</p>
                    </div>
                    <div className="col-sm-12 text-center">
                        <button type="button" className="btn btn-primary"
                                style={{borderColor: '#f25a08', backgroundColor: '#f25a08', letterSpacing: 2}}
                                onClick={this.exit}
                                > Salir
                        </button>
                    </div>
                    <div className="col-sm-12" dangerouslySetInnerHTML={{__html: '&nbsp;'}} />
                    <div className="col-sm-12 text-center">
                        <a href={$('meta[name=quiz-rules]').attr("content")} className="text-white">Consulta bases y condiciones</a>
                    </div>
                    <div className="col-sm-12" dangerouslySetInnerHTML={{__html: '&nbsp;'}} />
                    <div className="col-sm-12 text-center">
                        <a href="https://www.facebook.com/americancrewmexico" className="text-white"><i style={{fontSize:'2rem'}} className="fab fa-facebook-square"></i></a>
                    </div>

                </div>
            </div>
        )
    }
}

const AnswerBox = ({answer, onClick, length}) => {
    if(length  === 3)
        return(
            <Fragment>
                <div className="d-none d-md-block d-lg-none col-sm-4"></div>
                <div className="col-sm-4">
                    <img src={answer.asset_image} className="img-fluid" onClick={onClick}/>
                    <p style={{marginTop:12}} className="text-center quiz quiz-title">{ answer.text }</p>
                </div>
                <div className="d-none d-md-block d-lg-none col-sm-4"></div>
            </Fragment>
        );
    if(length === 2)
        return(
            <Fragment>
                <div className="col-1"/>
                <div className="col">
                    <img src={answer.asset_image} className="img-fluid" onClick={onClick}/>
                    <p style={{marginTop:12}} className="text-center quiz quiz-title">{ answer.text }</p>
                </div>
                <div className="col-1"/>
            </Fragment>
        );
    return null;
};

const Loader = () => (
    <div className="lds-css ng-scope">
        <div className="lds-blocks" style={{width:'100%',height:'100%'}}>
            <div style={{left:17,top:17,animationDelay:'0s'}}></div>
            <div style={{left:59,top:17,animationDelay:'0.049999999999999996s'}}></div>
            <div style={{left:101,top:17,animationDelay:'0.09999999999999999s'}}></div>
            <div style={{left:143,top:17,animationDelay:'0.15s'}}></div>
            <div style={{left:17,top:59,animationDelay:'0.5499999999999999s'}}></div>
            <div style={{left:143,top:59,animationDelay:'0.19999999999999998s'}}></div>
            <div style={{left:17,top:101,animationDelay:'0.5s'}}></div>
            <div style={{left:143,top:101,animationDelay:'0.25s'}}></div>
            <div style={{left:17,top:143,animationDelay:'0.44999999999999996s'}}></div>
            <div style={{left:59,top:143,animationDelay:'0.39999999999999997s'}}></div>
            <div style={{left:101,top:143,animationDelay:'0.35000000000000003s'}}></div>
            <div style={{left:143,top:143,animationDelay:'0.3s'}}></div>
        </div>
    </div>
);

if (document.getElementById('example')) {
    ReactDOM.render(<Example />, document.getElementById('example'));
}
