<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMicrosecondsToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('microseconds')->default('0');
        });

        Schema::table('user_answers', function (Blueprint $table) {
            $table->string('microseconds')->default('0');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('microseconds');
        });

        Schema::table('user_answers', function (Blueprint $table) {
            $table->dropColumn('microseconds');
        });
    }
}
