<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('quiz.end');
})->middleware('guest')->name('index');

Route::get('/quiz', function() {
    return view('quiz.start');
})->name('quiz')->middleware('auth');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::resource('questions', 'QuestionController')->except('show');
Route::resource('questions.answers', 'AnswerController')->except('show', 'index');

Route::get('datatable/questions', 'DatatableController@questions')->name('datatable.questions');
Route::get('datatable/questions/{id}/answers', 'DatatableController@answers')->name('datatable.answers');
Route::get('datatable/users', 'DatatableController@users')->name('datatable.users');
Route::view('bases-y-condiciones', 'quiz.bases')->name('bases');
Route::view('salones-y-barberias-participantes', 'quiz.participantes')->name('participantes');
