<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;

class MakeAdmin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:admin {email?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $email = $this->argument('email');
        if(is_null($email)) $email = env('ADMIN_EMAIL');
        $admin = User::whereEmail($email)->first();
        if($admin) {
            $this->info("admin already created with email: $email");
        } else {
            $this->info('creating admin');
            $user = new User();
            $user->name = $email;
            $user->code = $email;
            $user->email = $email;
            $user->password = bcrypt(env('ADMIN_PASS'));
            $user->is_admin = true;
            $user->api_token = str_random(60);
            $user->estado = '';
            $user->save();
        }

    }
}
