<?php

namespace App\Console\Commands;

use App\Code;
use Illuminate\Console\Command;

class MakeCode extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:code {count}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $count = $this->argument('count');
        for($i = 0; $i < $count; $i++){
            $code = new Code();

            do{
                $value = str_random(12);
                $this->info($value);
            } while(Code::where('value', $value)->first() != null);

            $code->value = $value;
            $code->save();
        }
    }
}
