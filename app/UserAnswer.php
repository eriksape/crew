<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserAnswer extends Model
{
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
