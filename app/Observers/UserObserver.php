<?php

namespace App\Observers;

use App\Jobs\ProccessLocation;
use App\Jobs\ProcessTime;
use App\User;

class UserObserver
{
    /**
     * Handle the user "created" event.
     *
     * @param  \App\User  $user
     * @return void
     */
    public function created(User $user)
    {
        ProccessLocation::dispatch($user->id);
    }
}
