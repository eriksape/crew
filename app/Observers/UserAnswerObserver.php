<?php

namespace App\Observers;

use App\Jobs\ProcessScore;
use App\UserAnswer;

class UserAnswerObserver
{
    /**
     * Handle the user answer "created" event.
     *
     * @param  \App\UserAnswer  $userAnswer
     * @return void
     */
    public function created(UserAnswer $userAnswer)
    {
        ProcessScore::dispatch($userAnswer->id);
    }
}
