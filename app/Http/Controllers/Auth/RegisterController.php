<?php

namespace App\Http\Controllers\Auth;

use App\Code;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/quiz';

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        return redirect('/');
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
            'phone' => 'required|string',
            'edad' => 'required|string',
            'lugar_compra' => 'required|string',
            'nombre_estilista' => 'required|string',
            'code' => 'required|string|max:12|unique:users|exists:codes,value',
            'estado' => 'required|string'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $location = [
            'longitude' => $data['longitude'],
            'latitude' => $data['latitude'],
            'ip' => request()->ip()
        ];

        $user = new User();
        $user->name = $data['name'];
        $user->email = $data['email'];
        $user->password = Hash::make('');
        $user->edad = $data['edad'];
        $user->phone = $data['phone'];
        $user->lugar_compra = $data['lugar_compra'];
        $user->nombre_estilista = $data['nombre_estilista'];
        $user->code = $data['code'];
        $user->api_token = str_random(60);
        $user->location = $location;
        $user->microseconds = $data['microseconds'];
        $user->estado = $data['estado'];
        $user->save();

        $code = Code::where('value', $data['code'])->first();
        $code->delete();

        return $user;
    }
}
