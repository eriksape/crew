<?php

namespace App\Http\Controllers;

use App\Answer;
use App\Jobs\ProcessTime;
use App\Question;
use App\UserAnswer;
use Illuminate\Http\Request;

class QuizController extends Controller
{
    public function question(){
        $type = 'opcion';
        $answered_questions = auth()->user()->answered;
        if($answered_questions->count() >= 4){
            $type = 'abierta';
        }
        $question = Question::with('answers')
            ->where('question_type', $type)
            ->whereNotIn('id', $answered_questions->pluck('question_id'))
            ->get();

        if($question->count() === 0){
            $answered = auth()->user()->answered()->orderBy('created_at')->get();
            $user = auth()->user();

            if($user->time == 0){
                $last_answer = $answered->last();
                $seconds = $last_answer->created_at->diffInSeconds($user->created_at);
                $user->seconds = $seconds;
                $user->save();

                ProcessTime::dispatch($user->id);

                return response()->json([
                    'time' => date('H:i:s', mktime(0, 0, $seconds)),
                    'microseconds' => [
                        'start' => $user->microseconds,
                        'end' => $last_answer->microseconds,
                    ]
                ]);
            } else {
                return response()->json([
                   'time' =>  date('H:i:s', mktime(0, 0, $user->seconds)) .  ltrim($user->time  - $user->seconds, 0),
                ]);
            }

        }
        if($question->count() === 1){
            $question->first()->last = true;
        }
        return response()->json($question->random(1)->first());
    }

    public function answer(Request $request){
        $answer = Answer::with('question')->find($request->answer);
        $userAnswer = new UserAnswer();
        $userAnswer->question_id = $answer->question_id;
        $userAnswer->microseconds = $request->microseconds;
        $userAnswer->answer_id = $answer->id;
        $userAnswer->user_id = auth()->id();
        $userAnswer->is_correct = $answer->is_correct;
        if($answer->question->question_type == 'abierta')
        {
            $userAnswer->answer = $request->text;
        }
        if($userAnswer->save()) return response()->json([]);
    }
}
