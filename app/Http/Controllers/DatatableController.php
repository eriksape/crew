<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class DatatableController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('auth.admin');
    }

    public function users()
    {
        $model = \App\User::with('open_answer')
            ->where('is_admin', 0)
            ->orderBy('score', 'desc')
            ->orderBy('time', 'asc')
            ->select('name', 'estado', 'code', 'time', 'score', 'id', 'lugar_compra', 'nombre_estilista', 'edad', 'phone', 'email');
        return DataTables::eloquent($model)
            ->editColumn('open_answer', function($row){
                return is_null($row->open_answer) ? null : $row->open_answer->answer;
            })
            ->toJson();
    }

    public function questions()
    {
        return DataTables::eloquent(\App\Question::query())
            ->addColumn('options', function($row){
                return view('questions.options', compact('row'))->render();
            })
            ->addColumn('details_url', function($row){
                return route('datatable.answers', $row->id);
            })
            ->rawColumns(['options'])
            ->toJson();
    }

    public function answers($id)
    {
        $model = \App\Answer::where('question_id', $id);
        return DataTables::eloquent($model)
            ->addColumn('is_correct', function($row){ return $row->is_correct; })
            ->addColumn('img', function($row){
                return view('answers.image', compact('row'))->render();
            })
            ->addColumn('options', function($row){
                return view('answers.options', compact('row'))->render();
            })
            ->rawColumns(['img', 'options'])->toJson();
    }
}
