<?php

namespace App\Http\Controllers;

use App\Answer;
use App\Question;
use Illuminate\Http\Request;

class AnswerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('auth.admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Question $question)
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Question $question)
    {
        return view('answers.form', compact('question'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $question)
    {
        $answer = new Answer();
        $answer->question_id = $question;
        $answer->image = $request->image->store('images');
        $answer->text = $request->text;
        $answer->is_correct = $request->get('is_correct');
        if($answer->save()){
            return redirect()->route('questions.answers.edit', ['question' => $question, 'answer' => $answer])->with('saved', true);
        } else return redirect()->route('questions.answers.create')->with('saved', false);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Question $question, Answer $answer)
    {
        return view('answers.form', compact('question', 'answer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $question
     * @param Answer $answer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $question, Answer $answer)
    {
        $answer->question_id = $question;
        if($request->image) $answer->image = $request->image->store('images');
        $answer->text = $request->text;
        $answer->is_correct = $request->get('is_correct');

        if($answer->save()){
            return redirect()->route('questions.answers.edit', ['question' => $question, 'answer' => $answer])->with('saved', true);
        } else return redirect()->route('questions.answers.create')->with('saved', false);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
