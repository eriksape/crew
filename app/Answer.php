<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    protected $casts = [
        'is_correct' => 'boolean'
    ];

    protected $appends = [ 'asset_image'];

    protected $hidden = ['is_correct'];

    public function getAssetImageAttribute()
    {
        return asset("app/$this->image");
    }

    public function question()
    {
        return $this->belongsTo(Question::class);
    }
}
