<?php

namespace App\Jobs;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ProcessTime implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $user_id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user_id)
    {
        $this->user_id = $user_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $user = User::find($this->user_id);
        $client = new \GuzzleHttp\Client();
        $answered = $user->answered()->orderBy('created_at')->get();
        $last_answer = $answered->last();
        $service = env('MICROSECONDS_SERVICE');
        $url = "$service?seconds=$user->seconds&start=$user->microseconds&end=$last_answer->microseconds";
        info($url);
        $res = $client->request('GET', $url);
        $body =  (array)json_decode($res->getBody());

        $user->time = $body['time'];
        $user->save();
    }
}
