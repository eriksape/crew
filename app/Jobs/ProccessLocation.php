<?php

namespace App\Jobs;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ProccessLocation implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $user_id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user_id)
    {
        $this->user_id = $user_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $user = User::find($this->user_id);

        if($user->location){
            if(is_null($user->location['latitude']) && is_null($user->location['longitude'])){
                $fromIP = geoip($user->location['ip']);
                $user->location = [
                    'ip' => $user->location['ip'],
                    'longitude' => $fromIP->lon,
                    'latitude' => $fromIP->lat,
                ];
                $user->save();
            }
            $longitude = $user->location['longitude'];
            $latitude = $user->location['latitude'];
            $client = new \GuzzleHttp\Client();
            $mapbox = env('MAPBOX_KEY');
            $res = $client->request('GET', "https://api.mapbox.com/geocoding/v5/mapbox.places/$longitude,$latitude.json?access_token=$mapbox");
            $user->location = [
                'ip' => $user->location['ip'],
                'longitude' => $user->location['longitude'],
                'latitude' => $user->location['latitude'],
                'data' =>  (array) json_decode($res->getBody())
            ];
            $user->save();
        }

    }
}
