<?php

namespace App\Jobs;

use App\UserAnswer;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ProcessScore implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $userAnswerId;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($userAnswerId)
    {
        $this->userAnswerId = $userAnswerId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $userAnswer = UserAnswer::find($this->userAnswerId);
        if($userAnswer->is_correct){
            $user = $userAnswer->user;
            $user->score = $user->score + 1;
            $user->save();
        }
    }
}
